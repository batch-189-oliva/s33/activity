// 1.
let toDoListItems = fetch('https://jsonplaceholder.typicode.com/todos')
					.then((response) => response.json())
					.then((data) => console.log(data))

console.log(toDoListItems)

// 2.
fetch('https://jsonplaceholder.typicode.com/todos')
					.then((response) => response.json())
					.then((data) => {
						console.log(data.map((toDoList) => {
							return {
								"title" : toDoList.title
							}
						}))	
					})

// 3.
fetch('https://jsonplaceholder.typicode.com/todos/1')
					.then((response) => response.json())
					.then((data) => { console.log(data)
					console.log(`The item "${data.title}" on the list has a status of ${data.completed}`)})


// 4.
fetch('https://jsonplaceholder.typicode.com/todos', {
	method: 'POST',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		userId: 1,
		id: null,
		title: 'Created To Do List Item',
		completed: false
	})
})
.then((response) => response.json())
.then((data) => console.log(data))

// 5.
fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PUT',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		dateCompleted: 'Pending',
		description: 'To Update the my to do list with a different data structure',
		id: null,
		status: 'Pending',
		title: 'Updated To Do List Item',
		userId: 1
	})
})
.then((response) => response.json())
.then((data) => console.log(data))

// 6. 
let today = new Date().toLocaleDateString();
fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PATCH',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		completed: false,
		dateCompleted: today,
		id: 1,
		status: 'Complete',
		title: 'delectus aut autem',
		userId: 1
	})
})
.then((response) => response.json())
.then((data) => console.log(data))

//  7.
fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'DELETE',
})
.then((response) => response.json())
.then((data) => console.log(data))


